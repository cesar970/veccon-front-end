<?php

global $wp_query;

$slug = $wp_query->query['imovel-slug'];
/**
 * Check if unity is cached in session, if true, just bind data,
 * if not, make a request to API system to get unity information
 */
if(array_key_exists($slug, $_SESSION['imovel'])){

    $imovel = $_SESSION['imovel'][$slug];

} else {

    /**
     * Request http
     */
    $imovel = json_decode(file_get_contents(('http://api.veccon.com.br/properties/slug/get?slug='.$slug)));

}

?> <?php get_header() ?> <section><div class="single-carousel owl-carousel owl-theme position-relative" id="single-property-carousel-full"></div><div class="container pt-5 pb-5"><div class="row align-items-center"><div class="col-md-4"><div class="spotlight"><h2 class="mb-0" id="single-property-title"></h2></div><span id="single-property-price"></span><p class="mt-3 color-greyd single-property-adress"></p></div><div class="col-md-8 benefits owl-carousel owl-theme text-center justify-content-between color-greyd" id="single-property-datasheets-benefits"></div></div><div class="row align-items-center mt-md-5"><div class="spotlight col-md-4"><h2>Sobre o empreendimento</h2></div><p class="color-greyd mt-2 col-md-8" id="single-property-info"></p><div class="col-12 text-center mt-4"><div class="border-top-r mt-3 mb-3"><a href="/contato" class="color-black d-flex align-items-baseline justify-content-center">Tenho interesse<br><i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i></a></div></div></div></div><div class="bg-blued pt-5 pb-5 ficha-tecnica"><div class="container"><div class="spotlight"><h2 class="bg-red m-auto text-white pt-2 pb-2 pr-4 pl-4">Ficha técnica</h2></div><div class="row mt-md-4 pb-md-4" id="single-property-datasheets"></div></div></div><div class="localizacao"><div class="d-md-flex"><div class="col-md-5 d-md-flex align-items-center justify-content-end"><div class="col-md-8 float-md-right text-center text-md-left"><div class="spotlight color-greyd mt-4 mb-4 m-0"><h2 class="color-black">Localização<br>do imóvel</h2></div><p class="color-greyd single-property-adress"></p></div></div><div class="col-md-7 pr-0 pl-0 mt-4 mt-md-0" id="single-property-maps"></div></div><div class="d-flex flex-wrap-reverse flex-md-wrap" id="single-video-wrap"><div class="col-md-8 pr-0 pl-0"><div class="video" id="single-video"></div></div><div class="col-md-4"><div class="col-md-8 float-md-left text-center text-md-left"><div class="spotlight color-greyd mt-4 mt-md-5 mb-4 m-0"><h2 class="color-black">Vídeo<br>do imóvel</h2></div></div></div></div></div><!--  <div class="depoimentos m-md-auto pt-5">
   
        <div class="d-flex flex-md-wrap-reverse flex-wrap">
   
         <div class="depoimentos-carousel owl-carousel owl-theme">
   
              <div class="item position-relative">
   
                 <div class="video col-md-8">
   
                       <iframe width="100%" src="https://www.youtube.com/embed/rLA3DUJbZQc" frameborder="0"
                               allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                               allowfullscreen></iframe>
   
                   </div>
   
                   <div class="bg-blued pt-5 pb-5 position-relative info-blue col-md-6 float-md-right">
   
                      <div class="info container text-white">
   
                          <div class="mb-4">
   
                          <p class="mb-0">Dyego Ferreira</p>
                           <span>Sócio</span>
   
                          </div>
   
   
   
                         <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                             been
   
                       </div>
   
                  </div>
   
               </div>
   
               <div class="item position-relative">
   
                   <div class="video col-md-8">
   
                       <iframe width="100%" src="https://www.youtube.com/embed/rLA3DUJbZQc" frameborder="0"
                               allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                               allowfullscreen></iframe>
   
                   </div>
   
                   <div class="bg-blued pt-5 pb-5 position-relative info-blue col-md-6 float-md-right">
   
                      <div class="info container text-white">
   
                           <div class="mb-4">
   
                               <p class="mb-0">Dyego Ferreira</p>
                               <span>Sócio</span>
   
                           </div>
   
                           <i class="fab fa-facebook-f position-absolute"></i>
   
   
                           <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                               been
                               the industry’s standard dummy.</p>
   
                      </div>
   
                   </div>
   
               </div>
   
          </div>
   
           <div class="spotlight text-center text-md-left mt-5 mb-5 container col-md-4">
   
               <h2>Depoimentos</h2>
            <p class="color-blackm">Veja o que nossos clientes falam sobre nossos empreendimentos</p>
        </div>
        </div>
   
      </div> --><div class="documentacao mt-5 pt-md-5 pb-5"><div class="container"><div class="row align-items-center"><div class="col-md-7 text-center text-md-left"><p class="color-greym mb-4">DOCUMENTAÇÃO</p><div class="spotlight"><h2>Nossos projetos são criados por profissionais qualificados, visando sempre uma infraestrutura de alta qualidade. Confira abaixo mais detalhes sobre o produto.</h2></div><div class="download" id="single-property-download"></div></div><div class="col-md-5 d-none d-md-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/img-documentacao.png" alt=""></div></div></div></div></section> <?php get_footer() ?> <script>$(document).ready(function () {

        Single_Property(<?=$imovel->id?>);

    });</script>