<?php
if (!isset($social_icon_class)) {
    $social_icon_class = "";
}
?> <a href="https://www.facebook.com/veccon/" title="Facebook - Veccon" target="_blank"><i class="fab fa-facebook-f <?=$social_icon_class?> mr-3"></i></a> <a href="https://www.instagram.com/veccon.loteamentos/" title="Instagram - Veccon" target="_blank"><i class="fab fa-instagram <?=$social_icon_class?> mr-3"></i></a> <a href="https://www.linkedin.com/company/veccon" title="Linkedin - Veccon" target="_blank"><i class="fab fa-linkedin-in <?=$social_icon_class?> mr-3"></i></a> <a href="https://www.youtube.com/VeCCon100" title="YouTube - Veccon" target="_blank"><i class="fab fa-youtube <?=$social_icon_class?>"></i></a>