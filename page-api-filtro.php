<?php
header('Content-Type: application/json');

$args = array(
    'post_type' => array('imoveis'),
    'posts_per_page' => '-1',

);

$imoveis_list = array();

$imoveis_wp_query = new WP_Query($args);

if ($imoveis_wp_query->have_posts()) {

    while ($imoveis_wp_query->have_posts()) {

        $imoveis_wp_query->the_post();

        $post_type = get_post_type();

        $city = get_field("city") ."/" . get_field("state");

        $type = get_field("type");

        $filter_by = $_GET['filter'];

        switch ($filter_by) {
            case "city":
                $filter = $city;
                break;
            case "property_type":
                $filter = $type;
                break;
            default:
                $filter = $city;
        }


        if($_GET['is_sale'] == get_field("is_sale") OR $_GET['is_rent'] == get_field("is_rent")){

            if($_GET['property_type'] == $type OR isset($_GET['property_type']) == false){


                if($_GET['city'] == $city OR isset($_GET['city']) == false){


                    if(!in_array($filter, $imoveis_list)){

                        array_push($imoveis_list, $filter);

                    }



                }



            }



        }

    }

    wp_reset_postdata();
}

echo json_encode($imoveis_list);
?>