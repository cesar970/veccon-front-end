 $.validate({
	form: '#form-boleto',
	modules: 'location, date, security, file, brazil',
	lang: 'pt'
});
var options = {
 onKeyPress : function(cpfcnpj, e, field, options) {
	 var masks = ['000.000.000-000', '00.000.000/0000-00'];
	 var mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
	 $('.cpfcnpj').mask(mask, options);
 }
};

$('.cpfcnpj').mask('000.000.000-000', options);




