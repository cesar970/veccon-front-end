const API_HOST = "//api.veccon.com.br/";

const sidebarBox = document.querySelector('#box'),
    sidebarBtn = document.querySelector('#btn'),
    pageWrapper = document.querySelector('#page-wrapper');

sidebarBtn.addEventListener('click', event => {
    sidebarBtn.classList.toggle('active');
    sidebarBox.classList.toggle('active');
});


window.addEventListener('keydown', event => {

    if (sidebarBox.classList.contains('active') && event.keyCode === 27) {
        sidebarBtn.classList.remove('active');
        sidebarBox.classList.remove('active');
    }
});





//
// const btns = document.getElementsByClassName( 'btn-menu' );
// const widget_red = document.getElementsById("widget-red");
//
// for ( let btn of btns ) {
// 	btn.onclick = function() {
// 		widget_red.style.display = 'none';
//
// 		console.log(widget_red[0]);
// 	}
// }


var floatToBRL = function (numero) {

    var int = parseInt( numero.replace(/[\D]+/g,'') );

    var tmp = int+'';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if( tmp.length > 6 )
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return tmp;

};



$(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {

        $("#heading-s").addClass("heading-scroll");

    } else {
        $("#heading-s").removeClass("heading-scroll");

    }
});


$("#btn").click(function () {

    if ($("#btn").hasClass("active")) {
        $("#widget-red").fadeOut(100);
    } else {
        $("#widget-red").fadeIn();
    }
});


$(".widget-red").click(function () {

    $(".widget-blue").fadeIn();

});

$(".close").click(function () {

    $(".widget-blue").fadeOut();

});

//owl carousel

$(document).ready(function () {
    $('.center-carousel').owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 10,
        responsive: {
            600: {
                items: 1
            },

            1200: {
                margin: 80,
                items: 2
            }
        }
    });

});

$('.depoimentos-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fas fa-arrow-left next'></i>", "<div class='bg-red next text-white'>Próximo <i class='fas fa-arrow-right ml-3'></i></div>"],
    dots: true,
    responsive: {
        0: {
            dots: true,
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: true
        },
        1200: {

            dots: true,
            items: 1,
            nav: true
        }
    }
});


var Spotlight_Home = function () {


    $('.spotlight-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
        dots: true,
        responsive: {
            0: {
                dots: true,
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true
            },
            1200: {

                dots: true,
                items: 3,
                nav: true
            }
        }
    });

    // $.ajax({
    //     type: "GET",
    //     url: API_HOST + "spotlight-home/",
    //     dataType: "json",
    //     timeout: 90000
    // }).done(function (data) {
    //
    //     $("#spotlight-home").html("");
    //
    //     $.each (data, function (index) {
    //
    //         var titulo = data[index].titulo.split("|");
    //
    //         var html = '<a href="' + data[index].slug + '"><div class="item"> <div class="img-spotlight" style="background-image: url(//admin.veccon.com.br' + data[index].photo_path + data[index].photo_dir + '/' + data[index].photo + ')"></div> <div class="bg-greyd position-relative pt-3 pt-lg-4"> <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white"> <h3>' + titulo[0] + '</h3> </div> <div class="h-grey col-10 mt-2 pb-2 m-auto"> <p>' + data[index].cidade + '</p> <p>' + titulo[1] + '</p> <p>' + data[index].area_total + '</p> </div> </div> </div></a>';
    //
    //         $("#spotlight-home").append(html);
    //
    //     });
    //
    //     $('.spotlight-carousel').owlCarousel({
    //         loop: true,
    //         margin: 10,
    //         nav: true,
    //         navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
    //         dots: true,
    //         responsive: {
    //             0: {
    //                 dots: true,
    //                 items: 1,
    //                 nav: true
    //             },
    //             600: {
    //                 items: 2,
    //                 nav: true
    //             },
    //             1200: {
    //
    //                 dots: true,
    //                 items: 3,
    //                 nav: true
    //             }
    //         }
    //     });
    //
    // });

};

var Spotlight_Historia = function () {



    $('.spotlight-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
        dots: true,
        responsive: {
            0: {
                dots: true,
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true
            },
            1200: {

                dots: true,
                items: 3,
                nav: true
            }
        }
    });

    // $.ajax({
    //     type: "GET",
    //     url: API_HOST + "projects/",
    //     dataType: "json",
    //     timeout: 90000
    // }).done(function (data) {
    //
    //     $("#spotlight-historia").html("");
    //
    //     $.each (data, function (index) {
    //
    //         var titulo = data[index].nome.split("|");
    //
    //         var html = '<div class="item"> <div class="img-spotlight" style="background-image: url(//admin.veccon.com.br' + data[index].foto_path + data[index].foto_dir + '/' + data[index].foto + ')"></div> <div class="bg-greyd position-relative pt-3 pt-lg-4"> <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white"> <h3>' + titulo[0] + '</h3> </div> <div class="h-grey col-10 mt-2 pb-2 m-auto"> <p>' + data[index].cidade + '</p> <p>' + titulo[1] + '</p> <p>' + data[index].ano + ' Unidades</p> </div> </div> </div>';
    //
    //         $("#spotlight-historia").append(html);
    //
    //     });
    //
    //     $('.spotlight-carousel').owlCarousel({
    //         loop: true,
    //         margin: 10,
    //         nav: true,
    //         navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
    //         dots: true,
    //         responsive: {
    //             0: {
    //                 dots: true,
    //                 items: 1,
    //                 nav: true
    //             },
    //             600: {
    //                 items: 2,
    //                 nav: true
    //             },
    //             1200: {
    //
    //                 dots: true,
    //                 items: 3,
    //                 nav: true
    //             }
    //         }
    //     });
    //
    // });

};

var Single_Property = function (idProperties) {

    $.ajax({
        type: "GET",
        url: API_HOST + "properties/get/",
        dataType: "json",
        data: {idProperties:idProperties},
        timeout: 90000
    }).done(function (data) {

        console.log(data);

        var titulo = data.titulo.split("|");

        $("#single-property-title").html(titulo[1]);

        var adress = data.endereco + ", " + data.bairro + ", " +  data.cidade;

        $(".single-property-adress").html(adress);

        $("#single-property-maps").html('<iframe src="https://maps.google.com/maps?q=' + adress + '&t=&z=15&ie=UTF8&iwloc=&output=embed" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>');

        $("#single-property-info").html(data.descricao);

        if(parseFloat(data.valor) > 0){

            $("#single-property-price").append('<strong>Venda</strong> R$ ' + floatToBRL(data.valor) + "</br>");

        }

        if(parseFloat(data.valor_aluguel) > 0){

            $("#single-property-price").append('<strong>Aluguel</strong> R$ ' + floatToBRL(data.valor_aluguel) + "</br>");

        }

        if(data.video.length > 8){

            data.video = data.video.replace("https://www.youtube.com/watch?v=", "");

            $("#single-video").html('<iframe width="100%" src="https://www.youtube.com/embed/' + data.video + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');


        }else{

            $("#single-video-wrap").remove();

        }

        $.each (data.property_datasheets, function (index) {

            if(data.property_datasheets[index].destaque == 2){

                var html = '<div class="item"><div class="flex-wrap"><div class="icon"><img src="//admin.veccon.com.br/files/property_datasheet/icone/' + data.property_datasheets[index].icone_dir + '/' + data.property_datasheets[index].icone + '" alt=""></div><p>' + data.property_datasheets[index].titulo + '</p></div></div>';

                $("#single-property-datasheets-benefits").append(html);

            }else{

                var html2 = '<div class="item d-flex col-md-4 align-items-center mt-4"><div class="icon col-3"><img src="//admin.veccon.com.br/files/property_datasheet/icone/' + data.property_datasheets[index].icone_dir + '/' + data.property_datasheets[index].icone + '" alt=""></div><div class="info text-white">' + data.property_datasheets[index].titulo + '</div></div>';

                $("#single-property-datasheets").append(html2);

            }

        });

        var html1 = '<div class="item"> <div class="slide-single" style="background: url(//admin.veccon.com.br/files/property/banner/' + data.banner_dir + '/' + data.banner + ') center center no-repeat;"></div> </div>';

        $("#single-property-carousel-full").append(html1);


        $.each (data.property_photos, function (index) {

            var html = '<div class="item"> <div class="slide-single" style="background: url(//admin.veccon.com.br/files/property_photo/photo/' + data.property_photos[index].photo_dir + '/' + data.property_photos[index].photo + ') center center no-repeat;"></div> </div>';

            $("#single-property-carousel-full").append(html);

        });


        $.each(data.property_attachments, function (index) {

            var dl_title = "";

            if(data.property_attachments[index].file.startsWith("X1-")){

                dl_title = "Apresentação";

                var html = '<div class="border-top-r mt-3 mb-3"><a href="//admin.veccon.com.br/files/attachments/' + data.property_attachments[index].file + '" target="_blank" class="color-black d-flex align-items-baseline justify-content-between">Baixar ' + dl_title + '<br><i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i></a></div>';

                $("#single-property-download").append(html);

            }else if(data.property_attachments[index].file.startsWith("X2-")){

                dl_title = "Matrícula";

                var html = '<div class="border-top-r mt-3 mb-3"><a href="//admin.veccon.com.br/files/attachments/' + data.property_attachments[index].file + '" target="_blank" class="color-black d-flex align-items-baseline justify-content-between">Baixar ' + dl_title + '<br><i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i></a></div>';

                $("#single-property-download").append(html);

            }else if(data.property_attachments[index].file.startsWith("X3-")){

                dl_title = "Projeto";

                var html = '<div class="border-top-r mt-3 mb-3"><a href="//admin.veccon.com.br/files/attachments/' + data.property_attachments[index].file + '" target="_blank" class="color-black d-flex align-items-baseline justify-content-between">Baixar ' + dl_title + '<br><i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i></a></div>';

                $("#single-property-download").append(html);

            }else{

            }

        });


        $('.benefits').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
            dots: true,
            responsive: {
                0: {
                    dots: true,
                    items: 2,
                    nav: true,
                    margin: 10
                },
                600: {
                    items: 3,
                    nav: true
                },
                1200: {
                    margin: 20,
                    dots: true,
                    items: 4,
                    nav: true
                }
            }
        });

        $('.single-carousel').owlCarousel({
            items: 1,
            loop: true,
            margin: 10,
            responsive: {
                600: {
                    items: 1
                },

                1200: {
                    margin: 80,
                    items: 1
                }
            }
        });

    });

};

var Single_Properties_Search = function () {


    // $.ajax({
    //     type: "GET",
    //     url: API_HOST + "properties/",
    //     dataType: "json",
    //     data:{ property_type:property_type, city:city, type:type},
    //     timeout: 90000
    // }).done(function (data) {
    //
    //     $("#imoveis-list-2-empty, #imoveis-list-empty").addClass('d-none');
    //
    //     var list_1 = 0;
    //
    //     var list_2 = 0;
    //
    //     $.each (data, function (index) {
    //
    //         var titulo = data[index].titulo.split("|");
    //
    //         var html = '<a href="/' + data[index].slug + '" class="item col-md-4"><div class="img-spotlight" style="background-image: url(//admin.veccon.com.br' + data[index].photo_path + data[index].photo_dir + '/' + data[index].photo + ')"></div> <div class="bg-greyd position-relative pt-3 pt-lg-4"> <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white"> <h3>' + titulo[0] + '</h3></div> <div class="col-11 h-grey col-md-10 mt-2 pb-2 m-auto"> <p>' + data[index].cidade + '</p> <p>' + titulo[1] + '</p> <p>' + data[index].area_total + '</p></div></div></a>';
    //
    //         if(parseFloat(data[index].valor) == 0 && parseFloat(data[index].valor_aluguel) == 0){
    //
    //
    //
    //
    //             if(property_type == "" && city == "") {
    //
    //                 list_1++;
    //
    //                 $("#imoveis-list-2").append(html);
    //             }
    //
    //         }else{
    //
    //             list_2++;
    //
    //             $("#imoveis-list").append(html);
    //
    //
    //         }
    //
    //     });
    //
    //     if(list_1 == 0){
    //
    //         $("#titulo-breves").addClass('d-none');
    //
    //     }
    //
    //     if(list_2 == 0){
    //
    //         $("#imoveis-list-empty").removeClass('d-none');
    //
    //         $("#titulo-empreen").addClass('d-none');
    //
    //         $("#paragrafo").addClass('d-none');
    //
    //     }
    //
    // });




    var SITE_URL = "https://veccon.com.br";

    var form_search_param = $("#search-param");

    var el_imoveis_type = $("#imoveis-type");

    var el_property_type = $("#property_type");

    var el_city = $("#city");

    var option_loading = '<option value="" selected disabled>Carregando...</option>';

    var option_default = '<option selected disabled>Selecione</option>';




    el_imoveis_type.change(function () {

        var is_sale = 0;

        var is_rent = 0;

        var filter = "property_type";



        if(parseInt($(this).val())){

            is_rent = 1;

        }else{

            is_sale = 1;

        }

        el_property_type.html(option_loading);

        el_city.html(option_loading);

        $.ajax({
            type: "GET",
            url: SITE_URL + "/api-filtro/",
            dataType: "json",
            data:{ is_sale:is_sale, is_rent:is_rent, filter:filter},
            timeout: 90000
        }).done(function (data) {

            el_property_type.html(option_default);

            el_city.html(option_default);

            $.each (data, function (index, item) {

                el_property_type.append('<option value="' + item + '">' + item + '</option>');

            });

        });



    });

    el_property_type.change(function () {

        var is_sale = 0;

        var is_rent = 0;

        var filter = "city";


        if(parseInt(el_imoveis_type.val())){

            is_rent = 1;

        }else{

            is_sale = 1;

        }

        var property_type = el_property_type.val();

        el_city.html(option_loading);

        $.ajax({
            type: "GET",
            url: SITE_URL + "/api-filtro/",
            dataType: "json",
            data:{ is_sale:is_sale, is_rent:is_rent, property_type:property_type, filter:filter},
            timeout: 90000
        }).done(function (data) {

            el_city.html(option_default);

            $.each (data, function (index, item) {

                el_city.append('<option value="' + item + '">' + item + '</option>');

            });

        });



    });

    $("#imoveis-type, #property_type, #city").change(function () {

        var is_sale = 0;

        var is_rent = 0;

        if(parseInt(el_imoveis_type.val())){

            is_rent = 1;

        }else{

            is_sale = 1;

        }


        var form_data = '<input type="hidden" name="is_sale" value="' + is_sale + '"><input type="hidden" name="is_rent" value="' + is_rent + '">';


        var val_property_type =  el_property_type.val();

        if(val_property_type != null){

            form_data = form_data + '<input type="hidden" name="property_type" value="' + val_property_type + '">';

        }

        var val_city =  el_city.val();

        if(val_city != null){

            form_data = form_data + '<input type="hidden" name="city" value="' + val_city + '">';

        }

        form_search_param.html('');

        form_search_param.append(form_data);

    });

    el_imoveis_type.trigger("change");


    $("#form_search").submit(function(){

        build();

        return false;
    });


    function build() {

        var search_param = $("#search-param").serialize();

        $.ajax({
            type: "GET",
            url: SITE_URL + "/api-imoveis-list/",
            dataType: "json",
            data: search_param,
            timeout: 90000
        }).done(function (data) {

            $("#imoveis-list-2-empty, #imoveis-list-empty").addClass('d-none');

            $("#imoveis-list").html('<div class="col-12 text-center"><div class="spotlight mb-md-4"><h2 id="titulo-empreen">Empreendimentos em destaque</h2><p class="color-greyd" id="paragrafo">Selecione as informações nos filtros e encontre o imóvel do seu sonho.</p></div></div>');

            $("#imoveis-list-2").html('<div class="col-12 text-center"><div class="spotlight mb-md-4"><h2 id="titulo-breves">Futuros Lançamentos</h2></div></div>');

            var list_1 = 0;

            var list_2 = 0;

            $.each (data, function (index) {

                var html = '<a href="' + data[index].link + '" class="item col-md-4"><div class="img-spotlight" style="background-image: url(' + data[index].photo + ')"></div> <div class="bg-greyd position-relative pt-3 pt-lg-4"> <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white"> <h3 class="text-uppercase">' + data[index].type + '</h3></div> <div class="col-11 h-grey col-md-10 mt-2 pb-2 m-auto"> <p>' + data[index].city + '</p> ' + data[index].name + '<p>' + data[index].area_total + '</p></div></div></a>';

                if(!data[index].short_releases){

                    if(el_property_type.val() == null){

                        list_1++;

                        $("#imoveis-list-2").append(html);

                    }

                }else{

                    list_2++;

                    $("#imoveis-list").append(html);


                }

            });

            if(list_1 == 0){

                $("#titulo-breves").addClass('d-none');

            }

            if(list_2 == 0){

                $("#imoveis-list").html('<div class="col-12 text-center"><div class="spotlight mb-md-4"><h2 id="titulo-empreen">Empreendimentos em destaque</h2><p class="color-greyd" id="paragrafo">Selecione as informações nos filtros e encontre o imóvel do seu sonho.</p></div></div><div class="col-12 text-center"><p class="color-greyd">Nenhum imóvel encontrado.</p></div>')

            }

        });

    }

    build();

};

var Boleto_Single = function (cpf) {

    $.ajax({
        type: "GET",
        url: "https://nilo.deltasi.com.br/Ares/consulta/boletos",
        dataType: "json",
        data: {token: "SA47Da9y2HT4v3Cc0n122018", cid:"veccon", doc: cpf},
        timeout: 90000
    }).done(function (data) {

        var boleto_nome = "";



        if(data.result != "ok"){

            $(".dados-boleto").html('<div class="text-center col-12"><i class="fa fa-exclamation-triangle color-red mr-2"></i> ' + data.result + '</div>');

            $(".tabela-boletos").remove();



        }else{


            $.each (data.content, function (index) {

                boleto_nome = data.content[index]['nome-sacado'];

                var html = '<tr> <td scope="row" class="d-none d-md-block"><span class="color-red">' + data.content[index].status + '</span> <br>' + data.content[index]['numero-documento'] + '</td> <td>' + data.content[index]['data-vencimento'] + '</td> <td>R$ ' + data.content[index].valor + '</td> <td id="download"> <a class="bg-red text-white p-2 pt-3 pr-3 pl-3" href="https://nilo.deltasi.com.br' + data.content[index]['url-boleto'] + '" target="_blank"><i class="far fa-file-alt text-white"></i> <span>Boleto</span></a> </td> </tr>';

                $("#boletos-list").append(html);

            });

            $("#boleto-nome").html(boleto_nome);

            $("#cpf").html(cpf);


        }



    });

};

var Single_Area = function () {


    $.validate({
        form: '#form-cadastre-sua-area',
        modules: 'location, date, security, file, brazil',
        lang: 'pt'
    });


    $(".link-collapse").click(function() {

        var titulo = $(this).data("titulo");

        $(".titulo").html(titulo);

    });

    $("#link-map").click(function () {

        var video = document.getElementById("video-map");

        video.play();

    });

    $('.phone').mask('(00) 0000-00009');

    $('.phone').blur(function(event) {

        if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara

            $(this).mask('(00) 00000-0009');

        } else {

            $(this).mask('(00) 0000-00009');

        }

    });


    (function(L, document) {

        var map = L.map('map', {
            center: [-22.8244781, -47.270407999999975],
            zoom: 8,
            measureControl: true
        });

        L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }).addTo(map);

        map.on('measurefinish', function(evt) {

            writeResults(evt);

        });

        var getAddressGeoCode = function () {

            var geocoder = new google.maps.Geocoder();

            var address = $("#search-address").val();

            geocoder.geocode( { 'address': address}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    console.log(results[0]);

                    var latitude = results[0].geometry.location.lat();

                    var longitude = results[0].geometry.location.lng();

                    codeLatLng(latitude, longitude);

                    map.setView(new L.LatLng(latitude, longitude), 17);

                }else{

                    alert('Endereço não encontrado.');

                }

            });

        };

        function codeLatLng(lat, lng) {

            var latlng = new google.maps.LatLng(lat, lng);

            var geocoder = new google.maps.Geocoder();

            var city;

            var state;

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    // console.log(results)
                    if (results[1]) {
                        //formatted address
                        // alert(results[0].formatted_address);
                        //find country name
                        for (var i=0; i<results[0].address_components.length; i++) {
                            for (var b=0; b<results[0].address_components[i].types.length;b++) {

                                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                    //this is the object you are looking for
                                    state= results[0].address_components[i];
                                    break;
                                }
                            }
                            for (var b=0; b<results[0].address_components[i].types.length;b++) {

                                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                                if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
                                    //this is the object you are looking for
                                    city= results[0].address_components[i];
                                    break;
                                }
                            }
                        }

                        $("#cidade").val(city.long_name);

                        $("#estado").val(state.short_name);


                    } else {
                        alert("Endereço não encontrado.");
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });
        }

        $("#search-address").blur(function() {

            getAddressGeoCode();

        });

        function writeResults(results) {


            var element_coordenadas = $("#coordenadas");

            var element_area = $("#area");

            if(results.area > 0){

                element_area.val(results.area);


                $("#kmz-span").hide();

                var arr = results.points;

                element_coordenadas.val("");

                var coordenadas = element_coordenadas.val();

                for (var i = 0; i < arr.length; i++) {

                    coordenadas = coordenadas + arr[i].lng + ',' + arr[i].lat + ',0 ';

                    element_coordenadas.val(coordenadas);

                }


            }else{

                $("#kmz-span").show();

                element_coordenadas.val("");

            }

        }

    })(window.L, window.document);

    $(function(){

        // First register any plugins
        $.fn.filepond.registerPlugin(FilePondPluginImagePreview);

        // Turn input element into a pond
        $('.my-pond').filepond();

        // Set allowMultiple property to true
        $('.my-pond').filepond('allowMultiple', true);

        // Listen for addfile event
        $('.my-pond').on('FilePond:addfile', function(e) {
            console.log('file added event', e);
            var imgData = new FormData($('#filet')[0]);
            console.log(imgData);
        });

    });
    
};