<?php get_header() ?>

    <section>

        <div class="slide-home">

            <?php echo do_shortcode('[rev_slider alias="home"]'); ?>

        </div>

    </section>

    <section>

        <div class="support-channels">

            <div class="container">

                <div class="col-12">

                    <div class="row justify-content-center align-items-center">

                        <div class="col-md-4 col-lg-3 text-center text-md-left mb-2 mb-md-0">

                            <h2>Canais de <br>

                                atendimento

                            </h2>

                        </div>

                        <div class="col-md-8 col-lg-9 row justify-content-md-between">

                            <a href="tel:+551935143838" class="color-blackm">

                            <div class="d-flex align-items-center mb-3 mb-md-0">

                                <i class="fas fa-phone mr-3"></i>

                                <div class="info">

                                    <span>Dep. Comercial.</span> <br>
                                    (19) 3514.3838

                                </div>

                            </div>

                            </a>

                            <a href="https://api.whatsapp.com/send?phone=5519992573363" title="WhatsApp" target="_blank">

                                <div class="d-flex align-items-center mb-3 mb-md-0 color-blackm">

                                    <i class="fab fa-whatsapp mr-3"></i>

                                    <div class="info">

                                        <span>WhatsApp.</span> <br>
                                        (19) 9 9257.3363

                                    </div>

                                </div>

                            </a>

                            <a href="mailto:veccon@veccon.com.br" title="E-mail">

                                <div class="d-flex align-items-center color-blackm">

                                    <i class="far fa-envelope mr-3"></i>

                                    <div class="info">

                                        <span>E-mail.</span> <br>
                                        veccon@veccon.com.br

                                    </div>

                                </div>

                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section>

        <div class="empreendimentos pb-5">

            <div class="container">

                <div class="spotlight text-center mb-4 col-11 col-md-12 m-auto pt-5 pb-4">

                    <h2>Empreendimentos
                        em destaque</h2>

                </div>

                <div class="spotlight-carousel owl-carousel owl-theme position-relative" id="spotlight-home">


                    <?php

                    $args = array(
                        'post_type' => array('imoveis'),
                        'posts_per_page' => '5',
                        'orderby' => 'rand',
                        'meta_query' => array(
                            array(
                                'key'     => 'is_featured',
                                'value'   => 1,
                                'compare' => 'IN',
                            ),
                        ),

                    );

                    $imoveis_wp_query = new WP_Query($args);

                    if ($imoveis_wp_query->have_posts()) {

                        while ($imoveis_wp_query->have_posts()) {

                            $imoveis_wp_query->the_post();

                            $post_type = get_post_type();

                            ?>

                            <a href="<?=get_permalink();?>" title="<?= the_title();?>">

                                <div class="item">

                                    <div class="img-spotlight" style="background-image: url(<?= get_field("featured_image")?>)"></div>

                                    <div class="bg-greyd position-relative pt-3 pt-lg-4">

                                        <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white">

                                            <h3 class="text-uppercase"><?= get_field("type")?></h3>

                                        </div>

                                        <div class="h-grey col-10 mt-2 pb-2 m-auto"> <p><?= get_field("city") . "/" . get_field("state")?></p><?= get_field("name")?><p><?= get_field("area")?></p></div>

                                    </div>

                                </div>

                            </a>

                    <?php

                        }

                        wp_reset_postdata();
                    }
                    ?>


                </div>

                <div class="col-12 text-center mt-4">

                    <div class="border-top-r mt-3 mb-3">

                        <a href="imoveis" class="color-black d-flex align-items-baseline justify-content-center">Ver todos <br>

                            <i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i>

                        </a>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section>

        <div class="oportunity pb-4 pt-md-4">

            <div class="container">

                <div class="col-12">

                    <div class="row text-md-left text-center align-items-center">

                        <div class="col-md-7">

                            <div class="spotlight col-12 mb-3 col-md-9 pt-5 pl-0">

                                <h2>Oportunidades de
                                    negócios, investimentos
                                    e até de uma nova vida.
                                </h2>

                            </div>

                            <p class="col-md-10 pl-0 color-greyd">Desenvolvemos e construímos empreendimentos
                                residenciais e
                                empresariais, e temos mais de 20 anos de experiência neste mercado. Todos os nossos
                                projetos têm uma entrega completa de infraestrutura, sempre com um cuidado especial.</p>

                            <img class="d-md-none" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/sala-home.png" alt="">


                            <div class="d-flex mt-5 align-items-center flex-wrap-reverse flex-md-wrap justify-content-md-start justify-content-center">

                                <div class="border-top-r mt-3 mb-3">

                                    <a href="invista" class="color-black d-flex align-items-baseline justify-content-center">Saiba
                                        mais <br>

                                        <i class="fas fa-arrow-right color-black mt-0 pt-0 ml-4"></i>

                                    </a>

                                </div>

<!--                                <img class="ml-md-5 mb-4 pl-md-3 veccon-20"-->
<!--                                     src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/dist/img/veccon-20-anos.png"-->
<!--                                     alt="">-->

                            </div>

                        </div>

                        <div class="col-md-5">

                            <img class="d-none d-md-block"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/sala-home.png" alt="">

                        </div>

                    </div>

                </div>

            </div>

            <div class="center-carousel owl-carousel owl-theme position-relative mt-5">

                <div class="item">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/_MG_1146.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/_MG_1162.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/_MG_1189.jpg" alt="">

                </div>

            </div>

        </div>

    </section>

    <section>

        <div class="container mt-5 pt-md-2 pb-2 mb-5">

            <div class="col-12">

                <div class="row text-center text-md-left">

                    <div class="spotlight border-red-l text-center col-12 mb-3 col-md-3 pt-5 pb-4">

                        <h2>Política de qualidade</h2>

                    </div>

                    <div class="col-md-9 color-greyd">

                        <p>Através de nossos valores, desenvolver empreendimentos imobiliários que promovam o
                            desenvolvimento social e sustentável das regiões onde atuamos, proporcionando melhor
                            qualidade de vida aos nossos clientes e maiores retornos financeiros aos nossos parceiros,
                            estabelecendo assim uma relação duradoura e proporcionando sentimentos de satisfação e
                            felicidade a todos os nossos stakeholders.</p>

                        <p>Para isso, aprimoramos continuamente nossos processos de gestão e atendemos integralmente aos
                            objetivos da qualidade e requisitos legais aplicáveis ao nosso negócio.</p>

                    </div>

                </div>

            </div>

        </div>

    </section>

<?php get_footer() ?>

<script>

    $(document).ready(function () {

        Spotlight_Home();

    });

</script>
