<!--hmnn1835ag-->
<footer>

	<div class="bg-blued pt-5 pb-5 text-center text-md-left">

		<div class="container">

			<div class="col-12">

				<div class="row align-items-center">

					<div class="col-md-5">

						<p class="color-red boleto-2-via">2ª Via do Boleto</p>

						<p class="text-white">Cliente Veccon gere aqui a <span class="color-red">segunda via</span> do seu boleto.</p>

						<form id="form-boleto" method="get" action="/boleto">

<!--							<input type="text" class="col-md-11" data-validation="custom" data-validation-regexp="^([a-zA-Z ]+)$"  placeholder="Nome*" data-validation-error-msg-container="#error-dialog-name"> <br>-->
<!---->
<!--							<span id="error-dialog-name" class="text-white font-weight-bold"></span>-->
<!---->
<!--							<br>-->

							<input id="segundavia" type="text" class="col-md-11 cpfcnpj" name="cpf" data-validation="length" data-validation-length="14-18" placeholder="CPF/CNPJ" data-validation-error-msg-container="#error-dialog-cpf"> <br>

							<span id="error-dialog-cpf" class="text-white font-weight-bold"></span>

							<div class="col-12 pl-0">

								<div class="border-top-r mt-3 mb-3">

									<div class="d-flex align-items-baseline">

										<input type="submit" value="Gerar" class="send">

										<i class="fas fa-arrow-right text-white mt-0 pt-0"></i>

									</div>

								</div>

							</div>

						</form>

					</div>

					<div class="col-md-4 text-white">

						<p class="mb-0">ENDEREÇO</p>

						<p>Edifício Veccon Prime Center 15ºand. <br>
							Estr. Mun. Teodor Condiev 970, <br>
							Jardim Res. Veccon Sumaré – SP <br>
						CEP 13171-105</p>

					</div>

					<div class="col-md-3 text-white mt-2">

						<p class="mb-0">CONTATO</p>

						<p>
                            <a href="mailto:veccon@veccon.com.br" class="text-white">veccon@veccon.com.br</a><br>

							Administrativo (19) 3514.3800 <br>

						Dep. Comercial (19) 3514.3838</p>

					</div>

					<div class="col-12 text-center mt-2 mt-md-0">

						<div class="float-md-right socials-footer color-red">

							<?php
                                $social_icon_class = "color-red";

							    include "social.inc.php";
                            ?>

						</div>

					</div>


					<div class="col-12 text-center mt-4">

						<small class="text-white">	© Copyright Veccon 2019. <br>
						Todos os direitos reservados.</small>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="place-widget d-md-none"></div>

</footer>

<!-- END FOOTER -->

<?php wp_footer(); ?>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>

</body>

</html>



