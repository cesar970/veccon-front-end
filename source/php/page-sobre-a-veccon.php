<?php get_header() ?>


    <div class="sobre-a-veccon">

        <div class="slide"></div>

        <div class="container pt-4 pt-md-5 pb-3 pb-md-5">

            <div class="row align-items-center">

                <div class="col-md-5 d-none d-md-block">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/img-sobre-veccon.png"
                         alt="Apartamento">

                </div>

                <div class="col-md-6 ml-md-4 text-center text-md-left">

                    <div class="spotlight mb-4">

                        <h2>
                            Sobre a Veccon
                        </h2>

                        <h3>Mais que loteamentos, oferecemos oportunidades. Oportunidades de negócios, investimentos e
                            até de uma nova vida.</h3>
                    </div>

                    <p class="mt-md-5">Desenvolvemos e construímos empreendimentos residenciais e empresariais, e temos
                        mais de 20 anos
                        de experiência neste mercado. Todos os nossos projetos têm uma entrega completa de
                        infraestrutura, sempre com um cuidado especial na preservação das áreas verdes e nas áreas
                        dedicadas ao lazer.
                    </p>

                    <p>
                        O que é importante para você, também é para nós - e por isso cuidamos também com atenção
                        especial dos prazos de entrega de cada empreendimento.</p>

                </div>

            </div>


            <div class="row mt-3 mb-md-5 pt-md-4 mt-md-5">

                <div class="spotlight col-md-3 pr-md-0 text-center text-md-left">

                    <h2 class="mb-4">Realizações
                        Veccon</h2>

                </div>

                <div class="col-md-9 realizacoes d-md-flex justify-content-md-between text-center text-md-left">

                    <div class="d-md-flex align-items-center">

                        <div class="icon mr-md-2 mt-3 mt-md-0">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/icon-dinheiro.png"
                                 alt="Ícone de dinheiro">

                        </div>

                        <p>
                            Land Bank próprio <br>
                            de <strong>3 milhões de m²</strong>

                        </p>

                    </div>

                    <div class="d-md-flex align-items-center">

                        <div class="icon mr-md-2 mt-4 mt-md-0">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/icon-localizacao.png"
                                 alt="Ícone de localização">

                        </div>

                        <p>
                            Presente nos estados de <br><strong>SP e MG</strong>, em <strong>11 cidades</strong>

                        </p>

                    </div>

                    <div class="d-md-flex align-items-baseline">

                        <div class="icon mr-md-2 mt-4 mt-md-0">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/icon-veccon.png"
                                 alt="Ícone de Veccon">

                        </div>

                        <p>
                            + de <strong>20 anos</strong> <br>

                            de mercado

                        </p>

                    </div>

                </div>

            </div>

        </div>


         <div class="depoimentos m-md-auto pt-5">

          <div class="d-flex flex-md-wrap-reverse flex-wrap">

            <div class="depoimentos-carousel owl-carousel owl-theme">

                <?php

                // check if the repeater field has rows of data
                if( have_rows('depoimentos') ):

                    // loop through the rows of data
                    while ( have_rows('depoimentos') ) : the_row();

                ?>
                    <div class="item position-relative">

                            <div class="video col-md-8">

<!--                                <iframe width="100%" src="https://www.youtube.com/embed/y8D2-Xyssm4" frameborder="0"-->
<!--                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"-->
<!--                                        allowfullscreen></iframe>-->
                                <?=the_sub_field("youtube");?>

                            </div>

                            <div class="bg-blued pt-5 pb-5 position-relative info-blue col-md-6 float-md-right">

                                <div class="info container text-white">

                                    <div class="mb-4">

                                        <p class="mb-0">Depoimento <?=the_sub_field('nome');?></p>
                                        <span><?=the_sub_field('tipo_do_cliente');?></span>

                                    </div>

                                    <i class="fab fa-facebook-f position-absolute" style="cursor: pointer" onclick="window.open('<?=the_sub_field("facebook");?>','_blank');"></i>


                                    <p>“<?=the_sub_field('texto');?>"</p>

                                </div>

                            </div>

                        </div>


                <?php

                    endwhile;

                else :

                    // no rows found

                endif;

                ?>

            </div>

            <div class="spotlight text-center text-md-left container col-md-4 fort-block">

                <h2>Depoimentos</h2>

                <p class="color-blackm">Veja o que nossos clientes falam sobre nossos empreendimentos</p>

            </div>

            </div>

        </div>

        <div class="bg-light">

            <div class="container">

                <div class="col-12 pt-md-5">

                    <div class="row color-blackm text-center text-md-left pt-4 pb-5">

                        <div class="col-md-6">

                            <h2 class="color-black">
                                Missão<span class="color-red">.</span>
                            </h2>

                            <p class="mb-5">Desenvolver com excelência, oportunidades de investimentos, de negócios e de
                                uma nova vida
                                para
                                todas as pessoas com quem nos relacionamos.</p>

                            <h2 class="color-black">
                                Visão<span class="color-red">.</span>
                            </h2>

                            <p class="mb-5">Ser protagonista do desenvolvimento urbano nas regiões em que atuamos,
                                valorizando o
                                relacionamento humano e o cuidado com o meio ambiente.
                            </p>


                        </div>

                        <div class="col-md-6">

                            <h2 class="color-black">
                                Valor<span class="color-red">.</span>
                            </h2>

                            <p>A cada dia de trabalho, a cada escolha de negócio que fazemos, construímos bons
                                relacionamentos e
                                vivenciamos nossos valores:
                            </p>

                            <div class="row justify-content-center mt-4 text-left list-sobre">

                                <div class="col-md-6">
                                    <ul>
                                        <li>Sustentabilidade</li>
                                        <li>Comprometimento</li>
                                        <li>Credibilidade</li>
                                        <li>Transparência</li>
                                        <li>Inovação</li>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul>
                                        <li>Ética</li>
                                        <li>Profissionalismo</li>
                                        <li>Espírito de servir</li>
                                        <li>Valorização da equipe</li>
                                        <li>Respeito ao ser humano</li>
                                    </ul>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        
        <div class="container mt-4 pt-md-2 pb-2 mb-5">

            <div class="col-12">

                <div class="row text-center text-md-left">

                    <div class="spotlight border-red-l text-center col-12 mb-3 col-md-3 pt-5 pb-4">

                        <h2>Política de qualidade</h2>

                    </div>

                    <div class="col-md-9 color-greyd">

                        <p>Através de nossos valores, desenvolver empreendimentos imobiliários que promovam o
                            desenvolvimento social e sustentável das regiões onde atuamos, proporcionando melhor
                            qualidade de vida aos nossos clientes e maiores retornos financeiros aos nossos parceiros,
                            estabelecendo assim uma relação duradoura e proporcionando sentimentos de satisfação e
                            felicidade a todos os nossos stakeholders.</p>

                        <p>Para isso, aprimoramos continuamente nossos processos de gestão e atendemos integralmente aos
                            objetivos da qualidade e requisitos legais aplicáveis ao nosso negócio.</p>

                    </div>

                </div>

                <div class="row text-center text-md-left mt-4">

                    <div class="col-md-12 color-greyd text-justify">

                        <p>A Veccon deve garantir que os seus diretores e funcionários não se envolvam em qualquer atividade, prática ou comportamento que constitua uma infração nos termos das leis anticorrupção ou antilavagem de dinheiro de qualquer país em que realiza negócios. A Lei nº. 12.846/2013 (lei anticorrupção brasileira), do Código Penal Brasileiro (artigos 332 e 333), a Lei de Improbidade Administrativa por Ações (Lei nº. 8.429/1992), o Estatuto Nacional do Servidor Público Civil Federal (Lei nº. 8.027/1990) ou quaisquer outros regulamentos anticorrupção ou código de conduta devem ser aplicáveis aos funcionários públicos, os mesmos não deverão realizar quaisquer atividades que possam ser consideradas razoavelmente uma infração aos princípios da Administração Pública no Brasil ou que possam ser classificadas como atos de suborno e práticas de corrupção, no âmbito de qualquer regulamentação internacional do qual o Brasil é signatário.</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

<?php get_footer() ?>