<?php get_header() ?>

    <div class="boleto">

        <div class="slide">

            <h2 class="text-white">Boletos</h2>

        </div>

        <div class="container mt-5 mb-5 pt-md-5 pb-md-5">

            <div class="col-12">

            <div class="dados-boleto row align-items-center">

                <div class="col-md-4 col-9 row align-items-center">

                    <i class="far fa-user color-red mr-2"></i>

                    <div class="nome-cpf">

                        <span id="boleto-nome"></span><br>
                        <span id="cpf" class="color-greym"></span>

                    </div>

                </div>

                <div class="col-md-9 col-3 pr-0 pl-0">

                    <hr>

                </div>

                </div>

            </div>

            <div class="tabela-boletos mt-3 mt-md-5">

                <div class="d-flex align-items-center mb-1">

                    <i class="far fa-file-alt text-white color-red icon-fatura"></i>
                    <h3 class="ml-2 mb-0">Lista de faturas</h3>

                </div>

                <table class="table color-greyd">
                    <thead class="border-0">
                    <tr class="border-0 color-greym">
                        <th scope="col" class="border-0 d-none d-md-block ">Parcela</th>
                        <th scope="col" class="border-0">Vencimento</th>
                        <th scope="col" class="border-0">Valor atualizado</th>
                        <th scope="col" class="border-0 pl-0 pl-md-2">Download</th>
                    </tr>
                    </thead>

                    <tbody id="boletos-list"></tbody>

                </table>

            </div>

        </div>

    </div>


<?php get_footer() ?>

<script>

    $(document).ready(function () {

        Boleto_Single("<?=preg_replace("/[^0-9]/", "",$_GET['cpf']);?>");

    });

</script>
