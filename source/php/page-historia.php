<?php get_header() ?>

<div class="historia">

    <div class="slide"></div>

    <div class="container">

        <div class="row align-items-start justify-content-center pt-5 mt-md-5 pb-3 ">

            <div class="col-md-3 d-none d-md-block">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/veccon-20-anos.png"
                     alt="Apartamento">

            </div>

            <div class="col-md-8 ml-md-4 text-center text-md-left">

                <div class="spotlight mb-4">

                    <h2>
                        História
                    </h2>
                </div>

                <p class="mt-md-3">Durante esses anos a Veccon desenvolveu Loteamentos Residenciais e Empresariais que contribuíram com desenvolvimento social e sustentável das regiões que atuamos, cumprindo assim nossa missão de proporcionar novas oportunidades para todas as pessoas com quem nos relacionamos, baseando-se sempre nos nossos valores.
                </p>
            </div>

        </div>

    </div>

    <div class="empreendimentos pb-5 pt-md-5 pb-md-5">

        <div class="container">

            <div class="spotlight text-center mb-5">

                <h2>Empreendimentos realizados</h2>

            </div>

            <div class="spotlight-carousel owl-carousel owl-theme position-relative" id="spotlight-historia">


                <?php

                // check if the repeater field has rows of data
                if( have_rows('empreendimentos_realizados') ):

                    // loop through the rows of data
                    while ( have_rows('empreendimentos_realizados') ) : the_row();

                        ?>
                        <div class="item">

                            <div class="img-spotlight" style="background-image: url(<?=the_sub_field("imagem");?>)"></div>

                            <div class="bg-greyd position-relative pt-3 pt-lg-4">

                                <div class="bg-red type-spotlight col-8 col-lg-10 m-auto text-center text-white">

                                    <h3 class="text-uppercase"><?=the_sub_field("tipo");?></h3> </div> <div class="h-grey col-10 mt-2 pb-2 m-auto">

                                    <p><?=the_sub_field("cidade");?></p> <?=the_sub_field("nome");?> <p><?=the_sub_field("quantidade");?></p>

                                </div>

                            </div>

                        </div>

                    <?php

                    endwhile;

                else :

                    // no rows found

                endif;

                ?>

            </div>

        </div>

    </div>

    <div class="all-video position-relative">

        <div class="bg-blued position-absolute d-none d-md-block"></div>

        <div class="video-historia">

            <iframe src="https://www.youtube.com/embed/ceqWhgRxJMM" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>

        </div>

    </div>

    <div class="oportunity pb-5">

        <div class="center-carousel owl-carousel owl-theme position-relative mt-5">


            <div class="item">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/carousel-historia-2.jpg" alt="">

            </div>

            <div class="item">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/carousel-historia-3.jpg" alt="">

            </div>

            <div class="item">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/carousel-historia-4.jpg" alt="">

            </div>


        </div>

    </div>

</div>


<?php get_footer() ?>

<script>

    $(document).ready(function () {

        Spotlight_Historia();

    });

</script>