<?php get_header() ?>

    <div class="imoveis">

        <div class="slide">

            <h2 class="container text-white">Imóveis</h2>

        </div>

        <div class="container pt-5">

            <div class="col-12">

                <div class="text-center">

                    <form id="search-param">



                    </form>

                    <form action="" class="row justify-content-center" id="form_search">

                        <div class="col-5 col-md-2 pl-0 pr-0 position-relative">

                            <div class="border-lg">

                                <i class="fas fa-chevron-down"></i>

                                <select name="type" id="imoveis-type">

                                    <option value="0" selected>Comprar</option>

                                    <option value="1">Alugar</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-7 col-md-3 pl-0 pr-0 position-relative ">

                            <div class="border-lg ">

                                <i class="fas fa-chevron-down"></i>

                                <select name="property_type" id="property_type" class="border-none disabled">
                                    <option value="" selected disabled>Selecione</option>
                                </select>

                            </div>

                        </div>

                        <label for class="col-6 col-md-5 pl-0 pr-0 position-relative">

                            <div class="border-lg">

                                <i class="fas fa-chevron-down"></i>

                                <select name="city" id="city">
                                    <option value="" selected disabled>Selecione</option>
                                </select>

                            </div>

                        </label>

                        <div class="col-6 col-md-2  pl-0 pr-0 ">

                            <input type="submit" value="Pesquisar" id="search-submit">

                        </div>

                    </form>

                </div>

            </div>

            <div class="row mt-5 mb-5" id="imoveis-list">

                <div class="col-12 text-center">

                    <div class="spotlight mb-md-4">
                        <h2 id="titulo-empreen">
                            Empreendimentos em destaque
                        </h2>
                        <p class="color-greyd" id="paragrafo">Selecione as informações nos filtros e encontre o imóvel do seu sonho.</p>
                    </div>

                </div>

                <div class="col-12 text-center d-none" id="imoveis-list-empty">
                    <p class="color-greyd">Nenhum imóvel encontrado.</p>
                </div>


            </div>

            <div class="row mb-5 pb-4 lacamentos" id="imoveis-list-2">

                <div class="col-12 text-center">

                    <div class="spotlight mb-md-4">
                        <h2 id="titulo-breves">
                            Futuros Lançamentos
                        </h2>
                    </div>

                </div>

                <div class="col-12 text-center d-none" id="imoveis-list-2-empty">
                    <p class="color-greyd">Nenhum imóvel encontrado.</p>
                </div>

            </div>

        </div>

    </div>

<?php get_footer() ?>

<script>

    $(document).ready(function () {

        Single_Properties_Search();

    });

</script>
