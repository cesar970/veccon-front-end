<!DOCTYPE html>
<html lang="pt_BR">

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>

        <?php

        if (is_home()):

            echo 'Veccon Construindo Desenvolvimento';

        elseif (is_category()):

            single_cat_title();

        elseif (is_single()):

            single_post_title();

        elseif (is_page()):

            $slug = $wp_query->query['imovel-slug'];

            if($slug == null OR $slug == ""){

                echo single_post_title();

            }else{

                /**
                 * Check if imovel is cached in session, if true, just bind data,
                 * if not, make a request to API system to get imovel information
                 */
                if(array_key_exists($slug, $_SESSION['imovel'])){

                    $imovel = $_SESSION['imovel'][$slug];

                } else {

                    /**
                     * Request http
                     */
                    $imovel = json_decode(file_get_contents(('http://api.veccon.com.br/properties/slug/get?slug='.$slug)));

                }

                echo $imovel->title;

            }
            

        else:

            wp_title('', true);

        endif;

        ?>

    </title>

    <meta name="robots" content="index, follow"/>

    <!-- Favicon -->

    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-16.png" sizes="16x16"
          type="image/png">

    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-32.png" sizes="32x32"
          type="image/png">

    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-48.png" sizes="48x48"
          type="image/png">

    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-62.png" sizes="62x62"
          type="image/png">

    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-192.png" sizes="192x192"
          type="image/png">

    <style type="text/css">

        <?php

        $url = get_bloginfo('template_directory') . '/dist/css/style.css';

        $ch = curl_init();

        curl_setopt ($ch, CURLOPT_URL, $url);

        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);

        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

        $contents = curl_exec($ch);

        if(curl_errno($ch)):

            echo curl_error($ch);

            $contents = '';

        else:

            curl_close($ch);

        endif;

        if(!is_string($contents) || !strlen($contents)):



            $contents = '';

    endif;



    $contents = str_replace('../img/', get_bloginfo('template_directory') . '/dist/img/', $contents);



    $contents = str_replace('../fonts/', get_bloginfo('template_directory') . '/dist/fonts/', $contents);



    echo $contents;

    ?>

    </style>

    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85203692-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-85203692-1');
    </script>
    <?php wp_head(); ?>


</head>

<body>

<header class="position-fixed heading" id="heading-s">

    <div class="container">

        <div class="d-flex align-items-lg-baseline align-items-center position-relative">

            <div class="col-lg-2">

                <a href="<?php echo get_home_url(); ?>/">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo.png " alt="Logo Veccon">

                </a>

            </div>

            <div class="col-lg-10 d-lg-flex justify-content-end">

                <div class="item color-black d-none d-lg-block mr-3 ">
                    <a class="bg-white color-black p-2 pl-3 pr-3 border" href="tel:+551935143800">
                        <i class="fas fa-phone "></i> (19) 3514-3800
                    </a>
                </div>
                <div class="item color-black d-none d-lg-block mr-3">
                    <a class="bg-white color-black p-2 pl-3 pr-3 border"
                       href="https://api.whatsapp.com/send?phone=5519992573363" title="WhatsApp" target="_blank">
                        <i class="fab fa-whatsapp"></i> (19) 99257-3363
                    </a>
                </div>

                <div class="mr-lg-5 d-none d-lg-block">

                    <a href="<?php echo get_home_url(); ?>/imoveis" class="text-white mr-2"><i
                                class="fas fa-search"></i> Procure seu imóvel</a>

                </div>

                <div id="btn" class="btn-menu mt-lg-0">
                    <div id='top'></div>
                    <div id='middle'></div>
                    <div id='bottom'></div>
                </div>

                <div id="box">
                    <div id="items">
                        <div class="item"><a href="<?php echo get_home_url(); ?>/">Home</a></div>
                        <div class="item item-ex"><label>Institucional</label></div>
                        <ul class="ml-5">
                            <a href="<?php echo get_home_url(); ?>/sobre-a-veccon">
                                <li>Sobre a Veccon</li>
                            </a>
                            <a href="<?php echo get_home_url(); ?>/historia">
                                <li> História</li>
                            </a>
                            <a href="<?php echo get_home_url(); ?>/acoes-sociais">
                                <li> Ações sociais</li>
                            </a>
                        </ul>
                        <div class="item"><a href="<?php echo get_home_url(); ?>/imoveis">Imóveis</a></div>
<!--                        <div class="item"><a href="--><?php //echo get_home_url(); ?><!--/invista">Invista</a></div>-->
                        <div class="item"><a href="<?php echo get_home_url(); ?>/cadastre-sua-area">Cadastre sua
                                área</a></div>
                        <div class="item"><a href="<?php echo get_home_url(); ?>/contato">Contato</a></div>
                        <!--						<div class="item"><a href="-->
                        <?php //echo get_home_url(); ?><!--/carreira-na-veccon">Carreira na Veccon</a></div>-->
                        <div class="item"><a href="https://veccon365.sharepoint.com/HomeVeccon.aspx" title="Área restrita">Área
                                restrita</a></div>
<!--                        <div class="item"><a href="#segundavia">2ª Via do boleto</a></div>-->
                        <div class="item col-12">



                            <form id="form-boleto" method="get" action="/boleto" class="col-12">

                                <label for="segundavia">

                                2º via de boleto

                                </label>

                                <input id="segundavia" type="text" class="col-md-11 cpfcnpj" name="cpf" data-validation="length" data-validation-length="14-18" placeholder="CPF/CNPJ" data-validation-error-msg-container="#error-dialog-cpf"> <br>

                                <span id="error-dialog-cpf" class="text-white font-weight-bold"></span>

                                <div class="col-12 pl-0">

                                    <div class="border-top-r mt-3 mb-3">

                                        <div class="d-flex align-items-baseline">

                                            <input type="submit" value="Gerar" class="send">

                                            <i class="fas fa-arrow-right text-white mt-0 pt-0"></i>

                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>
                        <div class="item color-black d-lg-none">
                            <a class="bg-white" href="tel:+551935143800">
                                <i class="fas fa-phone "></i> (19) 3514-3800
                            </a>
                        </div>
                        <div class="item color-black d-lg-none">
                            <a class="bg-white" href="https://api.whatsapp.com/send?phone=5519992573363&text=" target="_blank">
                                <i class="fab fa-whatsapp"></i> (19) 99257-3363
                            </a>
                        </div>
                        <div class="item d-flex text-white">
                            <?php include "social.inc.php" ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</header>

<div id="widget-red">

    <a class="widget-red d-flex justify-content-center widget-consultor position-fixed align-items-center text-white bg-red p-3">

        <i class="fas fa-comment-alt text-white mr-2"></i>

        <div class="text-widget">

            Fale com o
            <span class="text-21">	consultor</span>

        </div>

    </a>

</div>

<div class="col-md-5 widget-blue contacts bg-blued text-white text-left pt-5 pb-5 position-fixed d-nonee">

    <div class="close">

        <i class="fas fa-times"></i>

    </div>

    <div class="col-12">

        <?php echo do_shortcode('[contact-form-7 id="39" title="Tenho interesse"]') ?>

    </div>

    <div class="col-12 row pl-md-4">

        <a class="d-flex text-white align-items-center justify-content-center mb-3"
           href="https://api.whatsapp.com/send?phone=5519992573363&text=" target="_blank">

            <div class="icon-red">

                <i class="text-white fab fa-whatsapp "></i>

            </div>

            <div class="pl-0">

                <div class="info">

                    <span>WhatsApp<b class="color-red">.</b></span> <br>
                    (19) 9 9257.3363

                </div>

            </div>

        </a>

        <a class="d-flex text-white align-items-center justify-content-center" href="mailto:veccon@veccon.com.br">

            <div class="icon-red">

                <i class="far fa-envelope"></i>

            </div>

            <div class="col-9 pl-0">

                <div class="info">

                    <span>E-mail<b class="color-red">.</b></span> <br>
                    veccon@veccon.com.br

                </div>

            </div>

        </a>

    </div>

</div>










