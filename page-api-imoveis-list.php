<?php
header('Content-Type: application/json');

$args = array(
    'post_type' => array('imoveis'),
    'posts_per_page' => '-1',

);

$imoveis_list = array();

$imoveis_wp_query = new WP_Query($args);

if ($imoveis_wp_query->have_posts()) {

    while ($imoveis_wp_query->have_posts()) {

        $imoveis_wp_query->the_post();

        $post_type = get_post_type();

        $imovel = new stdClass();

        $imovel->name = get_field("name");

        $imovel->area_total = get_field("area");

        $imovel->city = get_field("city")."/" . get_field("state");

        $imovel->photo = get_field("featured_image");

        $imovel->type = get_field("type");

        $imovel->short_releases = get_field("short_releases");

        $imovel->link = get_permalink();

        if($_GET['is_sale'] == get_field("is_sale") OR $_GET['is_rent'] == get_field("is_rent")){

            if($_GET['property_type'] == get_field("type") OR isset($_GET['property_type']) == false){

                if($_GET['city'] == get_field("city")."/" . get_field("state") OR isset($_GET['city']) == false){

                    array_push($imoveis_list, $imovel);

                }

            }

        }

    }

    wp_reset_postdata();
}

echo json_encode($imoveis_list);
?>