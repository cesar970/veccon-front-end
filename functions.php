<?php  

/* Disable WordPress Admin Bar for all users but admins. */
  show_admin_bar(false);

(add_theme_support('post-thumbnails'));


// include custom jQuery
function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', get_stylesheet_directory_uri() . "/source/components/jquery/dist/jquery.js", array(), null, true);

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');


//RewriteRules
function loadRewriteRules()
{
	global $wp_rewrite;

	/**
	 * Session works like cache to reduce requests
	 */
	if(!isset($_SESSION['imovel'])){
		$request = json_decode(file_get_contents('http://api.veccon.com.br/properties/slug'));
		foreach ($request AS $value) {

			if(strlen($value->slug) > 3){

				$_SESSION['imovel'][$value->slug] = $value;

			}

		}
	}

	$imovel = $_SESSION['imovel'];

	$my_rewrite_rules_array = array();

	foreach ($imovel as $value) {
		$my_rewrite_rules_array[$value->slug. "$"] = "index.php?pagename=template-single&imovel-slug={$value->slug}";
	}

	$wp_rules = get_option('rewrite_rules');

	foreach ($my_rewrite_rules_array as $rule => $redirect){

		if (!isset($wp_rules[$rule])) {
			$wp_rewrite->flush_rules();
			break;
		}

		if ($wp_rules[$rule] != $redirect) {
			$wp_rewrite->flush_rules();
			break;
		}

	}

	return $my_rewrite_rules_array;

}

$my_rewrite_rules_array = loadRewriteRules();

function my_rules_array($rules_array) {
	global $my_rewrite_rules_array;
	return $my_rewrite_rules_array + $rules_array;
}
add_action('rewrite_rules_array', 'my_rules_array');

function my_query_vars($query_vars) {
	array_push($query_vars,"imovel-slug");
	return $query_vars;
}
add_filter('query_vars', 'my_query_vars');



/**
 * Custom Post Imóveis
 */

function create_post_type()
{

	/**
	 * Imóveis
	 */
	register_post_type('imoveis',
		array(
			'labels' => array(
				'name' => __('Imóveis'),
				'singular_name' => __('Imóveis')
			),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => 'dashicons-admin-multisite',
		)
	);
}

add_action('init', 'create_post_type');

?>